<?php

namespace Migration\Database;

class Ucimc_oldDatabase extends Database {
    
    public static function db() {

        self::set_ini_file(__DIR__ .  "/../../config.ini");
        self::get_from_ini_file('ucimc_old');

        if( self::$DB ) {
            return self::$DB;
        } else {
            try {
                self::$DB = new \PDO(
                    self::$driver . ':dbname=' . self::$db_name .  ';host=' . self::$host, 
                    self::$db_user, 
                    self::$db_pass
                    );
                return self::$DB;
            } catch(\PDOException $e) {
                echo 'DB connection failed: ',  '<i>' . $e->getMessage() . '</i>', "\n";
            }
        }
    }

    protected function set_ini_file($path)
    {
        self::$ini_file = $path;
    }
}