<?php

namespace Migration\Database;

abstract class Database {
    protected static $DB;
    protected static $ini_file;
    protected static $host;
    protected static $db_name;
    protected static $db_user;
    protected static $db_pass;
    protected static $driver;

    abstract public static function db();

    abstract protected function set_ini_file($path);

    protected function get_from_ini_file($section) {
        $details = parse_ini_file(self::$ini_file, true);
        self::$host = $details[$section]['host'];
        self::$db_name = $details[$section]['db_name'];
        self::$db_user = $details[$section]['db_user'];
        self::$db_pass = $details[$section]['db_pass'];
        self::$driver = $details[$section]['driver'];
    }
}