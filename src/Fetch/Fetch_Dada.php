<?php

namespace Migration\Fetch;

use Migration\Database\Ucimc_dadaDatabase;

class Fetch_Dada implements FetchInterface {

/// TODO: Fetch data from docs, groups, info, media, and razorwires db tables.

    protected $db;
    protected $nodes;
    protected $info;
    protected $terms;
    protected $limit;

    public function __construct($limit = false)
    {
        $this->getDB();
        $this->limit = is_numeric($this->limit) ? (int) $limit : false;
    }
    
    protected function getDB()
    {
        $this->db = Ucimc_dadaDatabase::db();
    }

    public function getNodes()
    {
        $this->pull_nodes_from_db();
        $this->pull_nodes_terms();
        return $this->nodes;
    }

    public function get_terms()
    {
        return $this->terms;
    }

    protected function pull_nodes_from_db()
    {
        $this->getArticles();
        $this->setInfo();
        // $this->getDocs();
        // $this->getGroups();
        // $this->getMedia();
        // $this->getRazorwires();
    }

    protected function getArticles()
    {
        $nodes = $this->runQuery("SELECT * FROM articles WHERE displayable = 1");
        while($node = $nodes->fetch(\PDO::FETCH_OBJ))
            $this->nodes[$node->objectid] = $node;
    }

    protected function setInfo()
    {
        $info = $this->runQuery("SELECT objectid, created_datetime, modified_timestamp, shortname, title, body FROM info");
        while($item = $info->fetch(\PDO::FETCH_OBJ))
            $this->info[$info->objectid] = $item;
    }

    public function getInfo()
    {
        return $this->info;
    }

    protected function runQuery($sql)
    {
        $nodes = $this->db->prepare($sql);
        $nodes->execute();
        return $nodes;
    }

    protected function pull_nodes_terms()
    {
        foreach ($this->nodes as $node)
            $this->pull_node_terms($node->objectid);
    }

    protected function pull_node_terms($nid)
    {
        $terms = $this->db->prepare("
            SELECT objectid, deleted, catname, shortname, description, category_id
            FROM categories AS cat
            INNER JOIN hash_categories AS hc ON cat.objectid = hc.category_id
            WHERE hc.ref_id = ?");
        $terms->execute([$nid]);
        while($term = $terms->fetch(\PDO::FETCH_OBJ)) {
            $this->terms[$nid][] = $term;
            $this->nodes[$nid]->terms = $this->terms[$nid];
        }
    }
}