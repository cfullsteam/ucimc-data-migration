<?php

namespace Migration\Fetch;

use Migration\Database\Ucimc_oldDatabase;

class Fetch_Ucimc_Old extends D6_Fetch {

    protected function getDB()
    {
        $this->db = Ucimc_oldDatabase::db();
    }
}