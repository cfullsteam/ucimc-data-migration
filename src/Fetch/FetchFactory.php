<?php
declare(strict_types=1);

namespace Migration\Fetch;

class FetchFactory {

    static public function getFetcher(array $Post)
    {
        switch ($Post['action']) {
            case 'ucimc_old':
                return new Fetch_Ucimc_Old();
                break;
            case 'ucimc_org':
                return new Fetch_Ucimc_Org();
                break;
            case 'dadaIMC':
                return new Fetch_Dada();
                break;
            default:
                throw new \Exception("No suitable Fetch class found.  Line " . __LINE__ . " of " . __FILE__ . ". Post was: " . json_encode($Post));
        }
    }
}
