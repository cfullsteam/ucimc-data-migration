<?php


namespace Migration\Fetch;

interface FetchInterface
{
    public function getNodes();
}