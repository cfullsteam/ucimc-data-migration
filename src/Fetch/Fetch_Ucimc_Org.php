<?php

namespace Migration\Fetch;

use Migration\Database\Ucimc_orgDatabase;

class Fetch_Ucimc_Org extends D6_Fetch {

    protected function getDB()
    {
        $this->db = Ucimc_orgDatabase::db();
    }
}