<?php

namespace Migration\Fetch;

abstract class D6_Fetch implements FetchInterface {
    
    protected $db;
    protected $nodes;
    protected $terms;
    protected $limit;

    public function __construct($limit = false)
    {
        $this->getDB();
        $this->limit = is_numeric($this->limit) ? (int) $limit : false;
    }

    abstract protected function getDB();

    public function fetch_and_push()
    {
        $this->pull_nodes_from_db();
        foreach ($this->nodes as $node) {
            $Post = new PostToPush($node);
            $Post->push();
        }
    }

    public function getNodes()
    {
        // if(!empty($this->nodes))
        //     return $this->nodes;
        $this->pull_nodes_from_db();
        $this->pull_nodes_terms();
        return $this->nodes;
    }

    public function get_terms()
    {
        return $this->terms;
    }

    protected function pull_nodes_from_db()
    {
        $sql = "
            SELECT n.nid, n.vid, n.title, n.status, n.created, n.changed, n.promote, n.moderate, n.sticky, nr.body, nr.teaser
            FROM node AS n 
            INNER JOIN node_revisions AS nr ON n.vid = nr.vid 
            WHERE n.type = ? AND n.title NOT LIKE 'Test Story:%'
            ";
        
        if($this->limit)
            $sql .= "LIMIT {$this->limit}";

        $nodes = $this->db->prepare($sql);
        $nodes->execute(['story']);
        while($node = $nodes->fetch(\PDO::FETCH_OBJ))
            $this->nodes[$node->nid] = $node;
    }

    protected function pull_nodes_terms()
    {
        foreach ($this->nodes as $node)
            $this->pull_node_terms($node->nid, $node->vid);
    }

    protected function pull_node_terms($nid, $vid)
    {
        $terms = $this->db->prepare("
            SELECT tn.tid FROM term_node
            WHERE nid = ? AND vid = ?");
        $terms->execute([$nid, $vid]);
        while($term = $terms->fetch(\PDO::FETCH_OBJ)) {
            $this->terms[$nid][] = $term->tid;
            $this->nodes[$nid]->terms = $this->terms[$nid];
        }
    }
}