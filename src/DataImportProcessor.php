<?php
declare(strict_types=1);

namespace Migration;

use Migration\Fetch\FetchFactory;
use Migration\Push\PushFactory;
use Migration\Push\PushFromDadaFactory;

class DataImportProcessor
{
    protected $Fetcher;
    protected $PushClassName;
    protected $nodes;

    public function __construct(array $Post)
    {
        $this->Fetcher = FetchFactory::getFetcher($Post);
        $this->nodes = $this->Fetcher->getNodes();
        $this->import();
    }

    public function import()
    {
        $n = count($this->nodes);
        array_map([$this, 'pushNode'], $this->nodes);
        sprintf('<p>%d posts processed for import.', $n);

        if(get_class($this->Fetcher) == 'Migration\\Fetch\\Fetch_Dada')
            $this->importDadaExtras();
    }

    protected function pushNode(\stdClass $node)
    {
        $Pusher = PushFactory::getPusher($this->Fetcher, $node);
        $Pusher->push();
    }

    protected function importDadaExtras()
    {
        $extras = $this->Fetcher->getInfo();
        $n = count($extras);
        $extra_type = 'info';
        array_map(function ($node) use ($extra_type) {
            $Pusher = PushFromDadaFactory::getPusher($extra_type, $node);
            $Pusher->push();
        }, $extras);
        sprintf('<p>%d Dada Info posts processed for import.', $n);
    }
}
