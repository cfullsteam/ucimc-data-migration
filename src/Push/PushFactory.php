<?php

declare(strict_types=1);

namespace Migration\Push;

use Migration\Fetch\FetchInterface;

class PushFactory
{
    static public function getPusher(FetchInterface $fetcher, \stdClass $node)
    {
        $class_name = (get_parent_class($fetcher) ? get_parent_class($fetcher) : get_class($fetcher));
        switch ($class_name) {
            case 'Migration\\Fetch\\D6_Fetch':
                return new WPPushFromDrupal($node);
                break;
            case 'Migration\\Fetch\\Fetch_Dada':
                return new WPPushFromDada($node);
                break;
        }
    }
}