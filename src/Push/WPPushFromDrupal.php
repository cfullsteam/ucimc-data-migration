<?php

namespace Migration\Push;

class WPPushFromDrupal extends WPPush {

    public function __construct($node)
    {
        if(is_object($node)) {
            $this->node = $node;
        } else {
            error_log("node not an object, it was: " . serialize($node));
        }
    }

    protected function makePostArray()
    {
        $this->post = [
            'ID' => 0,
            'post_author' => $this->import_user_id,
            'post_date' => $this->makeWpDate('created_date'),
            'post_date_gmt' => $this->makeWpDateGmt('created_date'),
            'post_content' => $this->node->body,
            'post_title' => $this->node->title,
            'post_excerpt' => $this->node->teaser,
            'post_status' => $this->makeWpStatus(),
            'post_type' => 'post',
            'post_modified' => $this->makeWpDate('changed_date'),
            'post_modified_gmt' => $this->makeWpDateGmt('changed_date'),
            'post_category' => [$this->import_cat_id]
        ];
    }
}