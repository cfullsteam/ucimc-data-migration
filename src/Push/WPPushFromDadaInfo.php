<?php

namespace Migration\Push;

class WPPushFromDadaInfo extends WPPush {

    protected $info_import_cat_id;

    public function __construct($node)
    {
        $info_import_cat = (object) [
            'catname' => 'Dada Info',
            'description' => 'Imported from the info table in old Dada database.'
        ];
        $this->ensureImportCatExists($info_import_cat);
        $node->displayable = 1;
        parent::__construct($node);
    }

    protected function makePostArray()
    {
        array_push($this->node->term_ids, $this->import_cat_id);
        array_push($this->node->term_ids, $this->info_import_cat_id);
        $this->post = [
            'ID' => 0,
            'post_author' => $this->import_user_id,
            'post_date' => $this->makeWpDate('created_datetime'),
            'post_date_gmt' => $this->makeWpDateGmt('created_datetime'),
            'post_content' =>  $this->node->body,
            'post_title' => $this->node->title,
            'post_excerpt' => $this->node->shortname,
            'post_status' => $this->makeWpStatus(),
            'post_type' => 'page',
            'post_modified' => $this->makeWpDate('modified_timestamp'),
            'post_modified_gmt' => $this->makeWpDateGmt('modified_timestamp'),
            'post_category' => $this->node->term_ids
        ];
    }

    protected function setCreatedDate()
    {
        try {
            $this->created_date = new \DateTime('@'.$this->node->created_datetime, $this->timezone_obj);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to create a DateTime object: ', $e->getMessage(), "\n";
        }
    }

    protected function setChangedDate()
    {
        try {
            $this->changed_date = new \DateTime('@'.$this->node->modified_timestamp, $this->timezone_obj);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to create a DateTime object: ', $e->getMessage(), "\n";
        }
    }

    protected function makeWpStatus()
    {
        $this->node->status = $this->node->displayable;
        switch ($this->node->displayable) {
            case 1:
                return 'publish';
                break;
            
            default:
                throw new \Exception("Attempting to create a wp post status from non-1 value. The status is {$this->node->displayable}.");
        }
    }

    public function push()
    {
        if(is_object($this->info->terms)) {
            foreach($this->info->terms as $term)
                $this->ensureImportCatExists($term->catname);
        }
        parent::push();
    }

    protected function ensureImportCatExists($cat = false)
    {
        if(!is_object($cat)) {
            parent::ensureImportCatExists();
        } else {
            /// TODO: Double-check data...  Is $cat->catname == $term->catname in ::push() above?
            $cat_id = get_cat_ID($cat->catname);
            if($cat_id !== 0) {
                $this->node->term_ids[] = $cat_id;
                return;
            }
            $this->createImportCat($cat);
        }

    }

    protected function createImportCat($cat = false)
    {
        $cat_args = [
            'cat_name' => $cat->catname,
            'category_description' => $cat->description
        ];
        $cat_id = wp_insert_category($cat_args, true);

        if(is_wp_error($cat_id)) {
            $this->handleWPError($cat_id, "Failed to create import category '$cat_name'.", true);
        } else {
            $this->node->term_ids[] = $cat_id;
        }
        if($cat->catname == $this->info_import_cat->catname)
            $this->info_import_cat_id = $cat_id;
    }
}