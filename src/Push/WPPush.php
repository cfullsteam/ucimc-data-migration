<?php

namespace Migration\Push;

abstract class WPPush implements Push {

    protected $node;
    protected $post;
    protected $wp_date_format = 'Y-m-d H:i:s';
    protected $wp_timezone = 'America/Chicago';
    protected $import_user = 'ucimc-archive';
    protected $import_user_id = 7929;
    protected $need_to_strip_text = true;
    protected $timezone_obj;
    protected $created_date;
    protected $changed_date;
    protected $import_cat_name = 'UCIMC Archive';
    protected $import_cat_id;

    public function __construct($node)
    {
        if(is_object($node)) {
            $this->node = $node;
        } else {
            error_log("node not an object, it was: " . serialize($node));
        }

        $this->ensureImportCatExists();
        $this->ensureImportUserExists();
    }

    protected function ensureImportCatExists($cat_name = false)
    {
        if(!$cat_name)
            $cat_name = $this->import_cat_name;

        $cat_id = get_cat_ID($cat_name);
        if($cat_id !== 0) {
            $this->import_cat_id = $cat_id;
            return;
        }
        $this->createImportCat();
    }

    protected function createImportCat($cat_name = false)
    {
        if(!$cat_name)
            $cat_name = $this->import_cat_name;

        $cat_id = wp_create_category($cat_name);

        if(is_wp_error($cat_id)) {
            $this->handleWPError($cat_id, "Failed to create import category '$cat_name'.", true);
        } else {
            $this->import_cat_id = $cat_id;
        }
    }

    protected function ensureImportUserExists()
    {
        if($user = get_user_by('login', $this->import_user)) {
            $this->import_user_id = $user->ID;
            return;
        }
        $this->createImportUser();
    }

    protected function createImportUser()
    {
        $uid = wp_insert_user([
            'user_login' => $this->import_user,
            'user_email' => 'charles@fullsteamlabs.com',
            'role' => 'editor'
        ]);

        if(is_wp_error($uid)) {
            $this->handleWPError($uid, "Failed to create import user in WP.", true);
        } else {
            $this->import_user_id = $uid;
        }
    }

    public function push()
    {
        /// TODO: Find out what statuses we're getting that are not '1', because it looks like there might be 3,268 of those.

        if($this->node->status == 1) {
            $this->setTimezoneObj();
            $this->setCreatedDate();
            $this->setChangedDate();
            $this->makePostArray();
            if($this->need_to_strip_text)
                $this->stripTextOfWholePost();

            $result = wp_insert_post($this->post, true);
            if(is_wp_error($result)) {
                $this->handleWPError($result, 'wp_insert_post errors');
                echo "the post was: " . dbg($this->post);
            } else {
                echo "New post id: $result <br>";
            }
        } else {
            echo "Post not published.<br>";
        }
    }

    protected function setTimezoneObj()
    {
        try {
            $this->timezone_obj = new \DateTimeZone($this->wp_timezone);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to create a DateTimeZone object: ',  $e->getMessage(), "\n";
        }
    }

    protected function setCreatedDate()
    {
        try {
            $this->created_date = new \DateTime('@'.$this->node->created, $this->timezone_obj);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to creat a DateTime object: ', $e->getMessage(), "\n";
        }
    }

    protected function setChangedDate()
    {
        try {
            $this->changed_date = new \DateTime('@'.$this->node->changed, $this->timezone_obj);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to creat a DateTime object: ', $e->getMessage(), "\n";
        }
    }

    protected function makePostArray()
    {
        $this->post = [
            'ID' => 0,
            'post_author' => $this->import_user_id,
            'post_date' => $this->makeWpDate('created_date'),
            'post_date_gmt' => $this->makeWpDateGmt('created_date'),
            'post_content' => $this->node->body,
            'post_title' => $this->node->title,
            'post_excerpt' => $this->node->teaser,
            'post_status' => $this->makeWpStatus(),
            'post_type' => 'post',
            'post_modified' => $this->makeWpDate('changed_date'),
            'post_modified_gmt' => $this->makeWpDateGmt('changed_date'),
            'post_category' => [$this->import_cat_id]
        ];
    }

    protected function stripTextOfWholePost()
    {
        $this->post = array_map([$this, 'convertSpecialCharacters'], $this->post);
    }

    protected function convertSpecialCharacters($text)
    {

/// TODO: Rename this method to something like 'convert_special_characters' and use the examples at 
/// 	  http://toao.net/48-replacing-smart-quotes-and-em-dashes-in-mysql to see if we can get a 
///	  successful import without stripping anything out.  Noticed what looked like  missing quotes 
///	  and apostrophes in several places.
        $text = str_replace(
            array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
            array("'", "'", '"', '"', '-', '--', '...'),
            $text
            );
        $regex = '/
        (
            (?: [\x00-\x7F]                  # single-byte sequences   0xxxxxxx
            |   [\xC2-\xDF][\x80-\xBF]       # double-byte sequences   110xxxxx 10xxxxxx
            |   \xE0[\xA0-\xBF][\x80-\xBF]   # triple-byte sequences   1110xxxx 10xxxxxx * 2
            |   [\xE1-\xEC][\x80-\xBF]{2}
            |   \xED[\x80-\x9F][\x80-\xBF]
            |   [\xEE-\xEF][\x80-\xBF]{2}';
        $regex .= '){1,40}                          # ...one or more times
            )
            | .                                  # anything else
            /x';
        return preg_replace( $regex, '$1', $text );
    }

    protected function makeWpDate($date_name)
    {
        return $this->$date_name->format($this->wp_date_format);
    }

    protected function makeWpDateGmt($date_name)
    {
        $offset = $this->$date_name->getOffset();
        $gmt_unix = $this->$date_name->getTimestamp() + $offset;
        return date($this->wp_date_format, $gmt_unix);
    }

    protected function makeWpStatus()
    {
        switch($this->node->status) {
            case 1:
                return 'publish';
                break;
            default:
                throw new \Exception("Attempting to create a wp post status from non-1 value. The status is {$this->node->status}.");
        }
    }

    protected function handleWPError($error, $message, $exception = false)
    {
        $errors = $error->get_error_messages();
        if($exception) {
            throw new \Exception($message . '; WPErrors: ' . dbg($errors));
        } else {
            dbg($errors, $message);
        }
    }
}
