<?php
declare(strict_types=1);

namespace Migration\Push;


class PushFromDadaFactory
{
    static public function getPusher(string $data_type, \stdClass $node)
    {
        switch ($data_type) {
            case 'info':
                return new WPPushFromDadaInfo($node);
                break;
            default:
                return new WPPushFromDadaInfo($node);
//            case 'docs':
//            case 'groups':
//            case 'media':
//            case 'razorwires':
        }
    }
}
