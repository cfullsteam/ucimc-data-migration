<?php

namespace Migration\Push;

class WPPushFromDada extends WPPush {

    protected function makePostArray()
    {
        array_push($this->node->term_ids, $this->import_cat_id);
        $this->post = [
            'ID' => 0,
            'post_author' => $this->import_user_id,
            'post_date' => $this->makeWpDate('created_date'),
            'post_date_gmt' => $this->makeWpDateGmt('created_date'),
            'post_content' => $this->node->body,
            'post_title' => $this->node->heading,
            'post_excerpt' => $this->node->summary,
            'post_status' => $this->makeWpStatus(),
            'post_type' => 'post',
            'post_modified' => $this->makeWpDate('changed_date'),
            'post_modified_gmt' => $this->makeWpDateGmt('changed_date'),
            'post_category' => $this->node->term_ids
        ];
    }

    protected function setCreatedDate()
    {
        try {
            $this->created_date = new \DateTime('@'.$this->node->created_datetime, $this->timezone_obj);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to creat a DateTime object: ', $e->getMessage(), "\n";
        }
    }

    protected function setChangedDate()
    {
        try {
            $this->changed_date = new \DateTime('@'.$this->node->modified_timestamp, $this->timezone_obj);
        } catch(\Exception $e) {
            echo 'Caught exception attempting to creat a DateTime object: ', $e->getMessage(), "\n";
        }
    }

    protected function makeWpStatus()
    {
        $this->node->status = $this->node->displayable;
        switch ($this->node->displayable) {
            case 1:
                return 'publish';
                break;
            
            default:
                throw new \Exception("Attempting to create a wp post status from non-1 value. The status is {$this->node->displayable}.");
        }
    }

    public function push()
    {
        /// TODO: We need to make these all published based on the value
        /// in the Dada db since they are all published but node->status 
        /// does not indicate this currently.
        if($this->node->status == 1) {
            foreach ($this->node->terms as $term) {
                $this->ensureImportCatExists($term->catname);
            }
        }
        parent::push();
    }

    protected function ensureImportCatExists($cat = false)
    {
        if(!is_object($cat)) {
            parent::ensureImportCatExists();
        } else {
            $cat_id = get_cat_ID($cat->catname);
            if($cat_id !== 0) {
                $this->node->term_ids[] = $cat_id;
                return;
            }
            $this->createImportCat($cat);
        }

    }

    protected function createImportCat($cat = false)
    {
        $cat_args = [
            'cat_name' => $cat->catname,
            'category_description' => $cat->description
        ];
        $cat_id = wp_insert_category($cat_args, true);

        if(is_wp_error($cat_id)) {
            $this->handleWPError($cat_id, "Failed to create import category '{$cat->catname}'.", true);
        } else {
            $this->node->term_ids[] = $cat_id;
        }
    }
}