# Data Migration Readme

WordPress plugin; reusable tool to import content into WordPress.  This project focused on importing content from Drupal 6 and DadaIMC databases that are accessible from WordPress.  The source databases need not have the same credentials as the WP database.

## Getting Started

1. Create config.ini in the plugin's root based on [example.config.ini](/example.config.ini).
1. For each source database create an extension of [Database.php](/src/Database/Database.php).  See the example below. 
1. Enable the plugin and look for _Tools > Data Migration_ in the admin menu, select your source, and click the button.

## Adding a new data source

1. Add a source to select input in [/templates/data-migration-admin.php](/templates/data-migration-admin.php), and to the switch in `FetchFactory::getFetcher()` in [FetchFactory.php](/src/Fetch/FetchFactory.php), 
1. Point that case to your class implementation(s) of [/src/Fetch.php](/src/Fetch.php).  See [/src/D6_Fetch.php](/src/D6_Fetch.php) for an example.
1. Add a Push class, see [WPPushFromDrupal.php](/src/Push/WPPushFromDrupal.php) for an example, and add it to the switch in `PushFactory::getPusher()` in [PushFactory.php](/src/Push/PushFactory.php).  
1. Refresh Data Migration page in admin UI.

### Example extension of Database abstract class

```php
<?php

namespace Migration\Database;

class Ucimc_orgDatabase extends Database {
    
    public static function db() {

        self::set_ini_file(__DIR__ .  "/../../config.ini");
        self::get_from_ini_file('ucimc_org');

        if( self::$DB ) {
            return self::$DB;
        } else {
            try {
                self::$DB = new \PDO(
                    self::$driver . ':dbname=' . self::$db_name .  ';host=' . self::$host, 
                    self::$db_user, 
                    self::$db_pass
                    );
                return self::$DB;
            } catch(\PDOException $e) {
                echo 'DB connection failed: ',  '<i>' . $e->getMessage() . '</i>', "\n";
            }
        }
    }

    protected function set_ini_file($path)
    {
        self::$ini_file = $path;
    }
}
```