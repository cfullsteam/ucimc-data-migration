<?php require_once __DIR__ . '/../process_import.php'; ?>

<section>
    <h1>Data Migration</h1>
    <form id="data-migration" method="post">
        <select name="action">
            <option value="ucimc_old">D6 UCIMC Old Import</option>
            <option value="ucimc_org">D6 UCIMC Org Import</option>
            <option value="dadaIMC">dadaIMC Import</option>
        </select>
        <input class="button button-primary" type="submit" name="migration-submit" value="Import">
    </form>
</section>
