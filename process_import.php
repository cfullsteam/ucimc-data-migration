<?php

use Migration\DataImportProcessor;

if(isset($_POST)) {
    $P = $_POST;
    if(isset($P['migration-submit'])) {
        if(isset($P['action'])) {
            new DataImportProcessor($P);
        } else {
            throw new \Exception("Data migration form submitted with no specified action");
        }
    }
}
