<?php

/**
 * @package UCIMC Data Migration
 */
/*
Plugin Name: UCIMC Data Migration
Plugin URI: https://www.fullsteamlabs.com
Description: Migrate old news content from Drupal and DadaIMC databases into WordPress.
Version: 0.2.2
Author: Charles Suggs <charles@fullsteamlabs.com>
Author URI: https://www.fullsteamlabs.com/
License: BSD-3
Text Domain: data-migration
*/

/*
 * 1. Get data from source db
 *    a) ucimc_old
 *    b) ucimc_org
 *    c) ucimc_dada
 * 2. Change data shape for import
 * 3. Import data
 * 4. Verify integrity of data
 */

init();

function init()
{
    require_once __DIR__ . '/src/Autoloader.php';
    spl_autoload_register(new \Migration\Autoloader('Migration', __DIR__ . '/src/'));
    add_action('admin_menu', 'data_migration_create_admin_page');
}

function data_migration_create_admin_page()
{
    add_management_page("Data Migration", "Data Migration", 'import', 'data-migration', 'data_migration_admin_page');
}

function data_migration_admin_page()
{
    require_once __DIR__ . '/templates/data-migration-admin.php';
}

function dbg($data, $title = false)
{
    if($title)
        print '<h1>' . $title . '</h1>';
    print '<pre>';
    print_r($data);
    print '</pre>';
}
